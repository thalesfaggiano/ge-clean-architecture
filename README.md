# GE-Clean Architecture

- [GE-Clean-Architecture](#ge-clean-architecture)
  * [Motivação](#motivação)
  * [Desafio](#desafio)
  * [Documentação](#documentação)

## Motivação

Este é um desafio da Pós-Graduação da Fullcycle: Go Expert, Desenvolvimento Avançado em Golang

## Desafio

Para este desafio, você precisará criar o usecase de listagem das orders.
Esta listagem precisa ser feita com:
- Endpoint REST (GET /order)
- Service ListOrders com GRPC
- Query ListOrders GraphQL

Não esqueça de criar as migrações necessárias e o arquivo api.http com a request para criar e listar as orders.

Para a criação do banco de dados, utilize o Docker (Dockerfile / docker-compose.yaml), com isso ao rodar o comando docker compose up tudo deverá subir, preparando o banco de dados.

Inclua um README.md com os passos a serem executados no desafio e a porta em que a aplicação deverá responder em cada serviço.

## Documentação


Este desafio foi construido em Golang, na versão 1.22.3. E embora o Golang seja retrocompatível, todos os testes foram realizados apenas nesta versão.

O Quickstart Docker foi realizado com o Bash em sua versão: 5.1.16 e as tecnologias com o docker nas versões:
```
Server: Docker Engine - Community
 Engine:
  Version:          26.1.1
  OS/Arch:          linux/amd64
 containerd:
  Version:          1.6.31
 runc:
  Version:          1.1.12
 docker-init:
  Version:          0.19.0
```

Para os exemplos de execução no Quickstart você deverá:

1 - Fazer download do código atualizado:

```bash
git clone git@gitlab.com:thalesfaggiano/ge-clean-architecture.git
```
E em seguida acessar seu diretório:

```bash
cd ge-clean-architecture
```

---
### Mapeamento das portas

| **Aplicação**  | **Porta** |
| -------------- | --------- |
| API            | 8000      |   
| Banco de dados | 3306      |   
| gRPC           | 50051     |   
| GraphQL        | 8080      |   

---
### Quickstart Docker Compose

Passo 1:

```bash
docker compose up -d
```
Passo 2:

```bash
make setup
```
Passo 3:

```bash
migrate -path ./migrations -database "mysql://root:root@tcp(localhost:3306)/orders?query" -verbose up
```
Passo 4:

```bash
make run
```

### Quickstart API HTTP/WEB

Em um novo terminal, execute:

Passo 5:

```bash
curl --location 'http://localhost:8000/order' \
--header 'Content-Type: application/json' \
--data '{
    "id": "abcd123",
    "price": 50.5,
    "tax": 0.12
}'
```

Passo 6:

```bash
curl --location 'http://localhost:8000/orders'
```

---
### Quickstart GraphQL

Acesse o GraphQL Playground e execute o seguinte exemplo de mutation e query:

Passo 7:

```bash
mutation createOrder {
  createOrder(input: {id: "abcd1234", Price: 50.5, Tax: 0.12}) {
    id
    Price
    Tax
    FinalPrice
  }
}
```

Passo 8:

```bash
query  {
  orders {
    id
    Price
    Tax
    FinalPrice
  }
}
```

---
### Quickstart gRPC

Em um novo terminal, execute:

Passo 9:

```bash
grpcurl -plaintext -d '{"id":"abcd1235","price": 100.5, "tax": 0.5}' localhost:50051 pb.OrderService/CreateOrder
```

Passo 10:

```bash
grpcurl -plaintext -d '{}' localhost:50051 pb.OrderService/ListOrders
```
